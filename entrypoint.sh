#!/bin/bash
set -e

apt update
apt install python3-pip -y

pip install flask 
cd /app 

flask run --host=0.0.0.0 --port=8000

