# Google Forms to Signal bridge bot

Notifies in signal group about new form posts

## Run

edit the `.env` file to adapt to your needs

`docker-compose up`

inside the container run:

signal-cli link -n "th-bridge"

then copy the displayed code and paste it in a QR-Code generator 
and scan this qr-code with your signal-app functionality add new linked device

