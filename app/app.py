from __future__ import print_function
import subprocess
from flask import Flask, request
import json
import sys
import os


GROUP_ID = os.getenv("SIGNAL_GROUP_ID")
SIGNAL_NUMBER = os.getenv("SIGNAL_NUMBER")
APP_ENDPOINT = os.getenv("APP_ENDPOINT")

app = Flask(__name__)


@app.route(f"/{APP_ENDPOINT}", methods=["GET", "POST"])
def send_to_group():
    if request.method == "GET":
        return "hello"

    print(request.data, file=sys.stdout, flush=True)

    data = request.get_json()
    message = (
        f"{data['Datum']}: {data['Wer']} hat {data['Betrag']} € mitgenommen, für {data['Kategorie']}"
    )
    if data["Kommentar"]:
        message += ": " + data["Kommentar"]

    try:
        print(subprocess.check_output(
            ["signal-cli", "-u", SIGNAL_NUMBER, "send", "-m", message, "-g", GROUP_ID]
        ).decode())
    except Exception:
        import traceback
        traceback.print_exc()

    return "message hopefully sent"

